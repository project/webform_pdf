<?php

/**
 * Implementation of hook_form_alter
 * Adds a "generate from pdf" button to the /node/add/webform page.
 */
function webform_pdf_form_alter($form_id, &$form){
	
	$button='<input type="button" value="Generate From PDF" 
				onclick="window.location.href=\'/webform_pdf/from_pdf\'"/><br/><br/>';
					
	if($form_id=='webform_node_form'){
		$form['from_pdf']=array(
			'#weight'=>-10,
			'#value'=>$button
		);
	}
}

/**
 * Implementation of hook_nodeapi
 */
function webform_pdf_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL){
	
	// on deleting a webform, clear all of its field-name values from the global variables
	if($op=='delete' && $node->type=='webform'){
		$orig_fields=variable_get('webform_pdf_orig_fields',array());
		unset($orig_fields[$node->nid]);
		variable_set('webform_pdf_orig_fields',$orig_fields);
	}
}

/**
 * This function generates the Webform from the uploaded PDF.  It first saves the PDF to /files,
 * then it calls a CGI script on MY home server which wget's YOUR saved PDF in order to parse the fields
 * through iText & send back the field-data necessary to create the webform.  
 * -- I want to change this! -- , but I can't seem to find any viable PHP solution for parsing PDF form-data, and I can't expect Drupal
 * users to have Java installed on their server.  Please email me at tylerrenelle@gmail.com if you know of
 * a PHP solution for parsing PDF form-fields, or have any other suggestions. 
 */
function webform_pdf_upload_form_submit($form_id, $form_values){
	$file = file_check_upload('pdf_upload');
	//handle the file, using file_save_upload, or something similar
	if (!$file) return;

	//save the node (we need the nid before uploading)
	$node->title=$file->filename;
	$node->type='webform';
	$node->webform=array( //defaults
	  'teaser' => 0,  'submit_text' => '',  'submit_limit' => -1,
      'submit_interval' => -1,  'email' => '',  'email_from_name' => 'default',
      'email_from_address' => 'default',  'email_subject' => 'default',  'additional_validate' => '',
      'additional_submit' => '',  'roles' => array(1, 2, 3),
	);
	node_save($node);
	
	$filename="webform_{$node->nid}.pdf";
	$file = file_save_upload($file,"pdf_forms/{$filename}");
	$host='http://'.$_SERVER['SERVER_NAME'];
	// get form fields from web service / CGI script
	$handle = fopen("http://ocdevel.com/cgi-bin/from_pdf.sh.cgi?form={$filename}&host={$host}", "rb");
	$contents = stream_get_contents($handle);
	fclose($handle);

	// parse XML
	$xml = simplexml_load_string( $contents );
	
	// webform_129.pdf => webform_129 (content-type name)
	$filename=substr($filename,0,-4);
	
	//need to preserve original names for exporting xfdf
	//might be better to store in its own table
	$orig_fields=variable_get('webform_pdf_orig_fields',array());
	
	//create fields
	$node->webform['components']=array();
	$i=1;
	$weight=-10;
	foreach($xml->field as $field){
		
		//strips out random crap in pdf field names.  I hate gov't pdf's, try naming fields, people!
		$drupal_fieldname =  strtolower( preg_replace('/[^a-zA-Z0-9]/', '_', (string)$field['name'])  );
			
		//$cleanUnicodeStr = trim(preg_replace('#[^\p{L}\p{N}]+#u', '', $fieldname));
		$orig_fields[$node->nid][$drupal_fieldname]=(string)$field['name']; //have to concat to cast to string
		
		
		// valid webform component types are:
            // date, email, fieldset, file, grid, hidden, markup, pagebreak, select,
            // textarea, textfield, time
		$field_type='';
		switch((int)$field['type']){
			case 5:	//List
			case 6:	//Combobox
					$field_type='select'; break;
			
			case 3:	//Radiobutton
			case 2: //Checkbox
					$field_type='select'; break;
			
			case 0: //None
        	case 1: //Pushbutton
					//return;
			
			case 4:	//Text
			case 7:	//Signature
			default: 
					$field_type='textfield'; break;
		}
		
		$node->webform['components'][$i]=
			array('cid'=>$i, 'nid'=>$node->nid, 'form_key'=>$drupal_fieldname, 
				'name'=>(string)$field['name'],'type'=>$field_type, 'weight'=>$weight++);
		if($field_type=='select'){
			$node->webform['components'][$i]['extra']['items'] = "0|Yes\n1|No";
		}	
		$i++;
	}	
	node_save($node);
		
	// save the original field names so we don't get f'd up with capitalization during xfdf export
	variable_set('webform_pdf_orig_fields',$orig_fields);
	
	// create dummy PDF so we know what fields are what
	/*drupal_execute('webform_client_form_'.$node->nid, $orig_fields[$node->title]);
	$sid=db_result(db_query("SELECT {sid} FROM {webform_submissions} WHERE nid=%d ORDER BY sid DESC"));*/
	 
	drupal_goto("node/{$node->nid}/edit");		
}

/**
 * Generates the PDF-ouput of the Webform
 * It first constructs the XFDF from the Webform's fields, then it sends that data to MY server (whereon is
 * saved YOUR previously uploaded PDF).  This XFDF is then merged (using PDFTK) with said PDF and flattened, 
 * then sent back to the browser.
 * -- I want to change this! --, but I can't seem to find any viable PHP solution for merging XFDF docs with
 * their respective PDFs and then flattening the document.  It's easy to just send the XFDF document to the
 * browser -- the user then gets a security warning that a remote PDF is trying to be accessed, and if this
 * PDF is encrypted, it will simply fail to populate.  Please email me at tylerrenelle@gmail.com if you know of
 * a PHP solution (non PDFTK, since I can't expect the user to have that available on their hosting) for 
 *  merging XFDF with PDF & flattening, or have any other suggestions. 
 */
function generate_pdf($nid, $sid){
		include_once(drupal_get_path('module','webform_pdf').'/xfdf.inc');
		include_once(drupal_get_path('module','webform').'/components/date.inc');
		$host='http://'.$_SERVER['SERVER_NAME'];
		
		$text_fields=array();
		$orig_fields=variable_get('webform_pdf_orig_fields',array());		
		//composite primary key= nid sid cid
		foreach($orig_fields[$nid] as $key=>$value){
			
			$component=db_fetch_object(db_query("SELECT type, cid FROM {webform_component} 
				WHERE nid=%d AND form_key='%s'", $nid,$key));
			if($component->type=='date'){
				$text_fields[$value]=_webform_pdf_render_date($nid,$sid,$component->cid);
			}
			else{
				$text_fields[$value]=db_result(db_query("SELECT s.data
					FROM {webform_submitted_data} s, {webform_component} c
					WHERE s.nid=%d AND s.sid=%d AND s.cid=c.cid AND c.form_key=\"%s\"", 
					$nid, $sid, $key));
			}
		}		
		
		$form_name = "webform_{$nid}";
		$pdf_file="{$host}/files/pdf_forms/{$form_name}.pdf";
    		
		// get the XFDF file contents	
		$xfdf=createXFDF($pdf_file,$text_fields);
				
		$file_name="{$form_name}.xfdf";		
		file_save_data($xfdf, "pdf_forms/{$file_name}", FILE_EXISTS_REPLACE);
		
		$download_name=db_result(db_query("SELECT title FROM {node} WHERE nid=%d",$nid));
		$download_name= preg_replace('/[^a-zA-Z0-9_]/','',$download_name).'.pdf';
		
		//header("Content-Type:application/pdf");		
		header('Content-type: application/vnd.adobe.xfdf');
		header('Content-disposition: attachment; filename='.$download_name);
		readfile("http://ocdevel.com/cgi-bin/to_pdf.sh.cgi?form={$form_name}&host={$host}");
		
		//header("Location: http://ocdevel.com/cgi-bin/to_pdf.sh.cgi?form={$form_name}&host={$host}");
	
}

function _pdf_webform_render_date($nid,$sid,$cid){
	$date=array();
	$query=db_query("select no, data from {webform_submitted_data} where nid=%d and sid=%d and cid=%d",$nid,$sid,$cid);
	while($data=db_fetch_object($query)){
		switch($data->no){
			case '0': $date['month']=$data->data;break;
			case '1': $date['day']=$data->data;break;		
			case '2': $date['year']=$data->data;break;				
		}
	}
	$timestamp=mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
	return date("M d, Y",$timestamp);
}

/*function webform_pdf_print_forms(){
	global $user;
	$uid=$user->uid;
		
	$query=db_query("SELECT s.sid, s.nid, s.submitted, n.title 
					FROM {node} n, {webform_submissions} s
					WHERE s.uid=%d AND s.nid=n.nid",$uid);
	while($result=db_fetch_object($query)){		
		$output.=l($result->title.'  ('.format_date($result->submitted, "small").')','webform_pdf/to_pdf/'.$result->nid.'/'.$result->sid);
		$output.='<br/>';	
	}
	$form['content']=array(
		'#value' => $output
	);
	return $form;
}*/
?>