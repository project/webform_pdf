<?php
/*function webform_pdf_cron(){
	// delete all created submissions (since they're not intuitively nodes), interim solution
	// until webform_cck is built
	$query = db_query("SELECT nid FROM {node} WHERE type='instant_eval' OR type='cr180'");
	while ($result = db_fetch_array($query)) {
	    node_delete($result['nid']);
 	}
	
}*/


/**
 * Implementation of hook_nodeapi().
 * redirect to pdf output upon node submission
 */
function webform_pdf_nodeapi(&$node, $op){
	if($op=='insert'){
		//$nid=$node->nid;		
		//drupal_goto("webform_pdf/pdf/$nid");
	}
}

function webform_pdf_form_alter($form_id, &$form){
	//print_r($form_id);
}



function webform_pdf_upload_form_submit($form_id, $form_values){
	$file = file_check_upload('pdf_upload');
	//handle the file, using file_save_upload, or something similar
	if (!$file){return;}
	$filename=$file->filename;
	$file = file_save_upload($file,"pdf_forms/$filename");
	// get form fields from web service / CGI script
	$handle = fopen("http://ocdevel.com/cgi-bin/webform_pdf.sh.cgi?q=$filename", "rb");
	$contents = stream_get_contents($handle);
	fclose($handle);
	

	// parse XML
	$xml = simplexml_load_string( $contents );
	
	// cr180.pdf => cr180 (content-type name)
	$filename=substr($filename,0,-4);
	
	//need to preserve original names for exporting xfdf
	//might be better to store in its own table
	$orig_fields=variable_get('webform_pdf_orig_fields',array());
	
	//require_once(drupal_get_path('module','webform_pdf').'/create_content_type.inc');
	$content[type]  = create_content_type($filename);
	
	//create fields
	$content[fields]  = array();
	$weight=-10;
	
	foreach($xml->field as $field){
		
		//strips out random crap in pdf field names.  I hate gov't pdf's, try naming fields, people!
		$drupal_fieldname =  strtolower( 'field_'.preg_replace('/[^a-zA-Z0-9]/', '_', (string)$field['name'])  );
		
		//$cleanUnicodeStr = trim(preg_replace('#[^\p{L}\p{N}]+#u', '', $fieldname));
		$orig_fields[$filename][$drupal_fieldname]=(string)$field['name']; //have to concat to cast to string
		
		$field['drupal_fieldname']=$drupal_fieldname;		
		$field['weight']=$weight;
		array_push($content[fields], create_field($field));
		
		// gotta find a way to bypass 20-weight limit, it matters.
		$weight=($weight++>10)?10:$weight;
	}	
	//drupal_goto('devel/variable');
		
	// save the original field names so we don't get f'd up with capitalization during xfdf export
	variable_set('webform_pdf_orig_fields',$orig_fields);
		
	// create the content type
	webform_pdf_create_content_type_from_array($content);
	
	// create dummy node to show field names & redirect
	//$node = new stdClass();
	$node->type = $filename;
	$values=array();
	foreach($orig_fields[$filename] as $key=>$value){ // key = field_blabla, value = BlaBlaBla
		$values[$key][0]['value'] = "{$value}";	
	}
	drupal_execute($filename.'_node_form', $values, $node);
	$nid=db_result(db_query("SELECT nid FROM {node} ORDER BY nid DESC")); //most recent node, aka the one we just added (shitty way to go about it)
	drupal_goto("webform_pdf/pdf/$nid");
			 
	//drupal_goto("admin/content/types/$filename");

}
  

function webform_pdf_create_content_type_from_array($content, $type_name = '<create>') {
  $type = $content['type']['type'];
  global $_webform_pdf_install_macro;
  $_webform_pdf_install_macro[$type] = $content;
  $macro = 'global $_webform_pdf_install_macro; $content = $_webform_pdf_install_macro['. $type .'];';
  drupal_execute('content_copy_import_form', array('type_name' => $type_name, 'macro' => $macro));
  content_clear_type_cache();
}


/**
 * Generates an XFDF and sends it to the browser
 */
function generate_pdf($nid){
	
		include_once(drupal_get_path('module','webform_pdf').'/xfdf.inc');
		$host='http://'.$_SERVER['SERVER_NAME'];
	
		/*	EXAMPLE DATA		
		{content_type_pdf_form}
		vid 	nid 	field_filltext58a_value
		3 		3 		teset

		{node_field_instance}
	  	field_name 			type_name 	label 			widget_type
		field_filltext58a 	pdf_form 	FillText58a 	text
		 	 
		{node}
		nid 	vid 	type 		title
		3 		3 		pdf_form 	test*/
		 
		$content_type=db_result(db_query("SELECT type FROM {node} WHERE nid=%d",$nid));
		/*$field_name=db_result(db_query("SELECT field_name FROM {node_field_instance} WHERE type_name='%s'",$content_type));
		$sql=db_query("SELECT f.field_name, f.label, f.widget_type, c.%s_value as value
				FROM {node_field_instance} f, {node} n, {content_type_%s} c
				WHERE f.type_name = '%s'",$field_name,$content_type,$content_type);*/
		
		$text_fields=array();
		/*while($result=db_fetch_object($sql)){
			$text_fields[ $orig_fields[ $result->field_name ] ]=$result['value'];
		}*/
		
		$orig_fields=variable_get('webform_pdf_orig_fields',array());
		$orig_fields=$orig_fields[$content_type];
		
		foreach($orig_fields as $key=>$value){
			$text_fields[$value]=db_result(db_query("
				SELECT %s_value
				FROM {content_type_%s}
				WHERE nid=%d",$key,$content_type,$nid
			));
		}			  
		
		     
		$pdf_file="$host/files/pdf_forms/$content_type.pdf";
    		
		// get the XFDF file contents	
		$xfdf=createXFDF($pdf_file,$text_fields);
		
		drupal_set_header('Content-type: application/vnd.adobe.xfdf');
		//drupal_set_header('Content-Disposition: attachment; filename="'.$content_type.'.xfdf"');
		drupal_set_header('Content-Disposition: inline; filename="'.$content_type.'.xfdf"');
		
		//Does the same thing but by saving a file first... someone said this is safer, whatever.
		/*$file_name="$content_type.xfdf";		
		file_save_data($xfdf, "pdf_forms/$file_name", FILE_EXISTS_REPLACE);
		readfile($host.'/files/pdf_forms/$content_type.xfdf');*/
		
		echo $xfdf;
		
		//this can be placed in .htaccess to avoid setting headers
		/* # Force PDF's to open without the plugin
		   <FilesMatch "\.(?i:pdf)$">
			   ForceType application/pdf
			   Header set Content-Disposition attachment
		   </FilesMatch>
		*/
}


function create_content_type($name){
	
	return array (
	  'name' => $name,
	  'type' => $name,
	  'description' => '',
	  'title_label' => 'Title',
	  'body_label' => '',
	  'min_word_count' => '0',
	  'help' => '',
	  'node_options' => 
	  array (
	    'status' => true,
	    'promote' => false,
	    'sticky' => false,
	    'revision' => false,
	  ),
	  'comment' => '0',
	  'upload' => '0',
	  'nodewords' => 0,
	  'old_type' => $name,
	  'orig_type' => '',
	  'module' => 'node',
	  'custom' => '1',
	  'modified' => '1',
	  'locked' => '0',
	  'ant' => '1',
	  'ant_pattern' => '',
	  'ant_php' => 0,
	);
}

function create_field($field){
		$tyler=variable_get('tyler',array());
		$fname=$field['drupal_fieldname'];
		$tyler[(string)$fname]=(int)$field['type'];
		variable_set('tyler',$tyler);//drupal_goto('devel/variable');	
		
		
			
        switch((int)$field['type']) {
     
			// valid webform component types are:
            // date, email, fieldset, file, grid, hidden, markup, pagebreak, select,
            // textarea, textfield, time


        	//Acrobat components
        		
			case 5:	//List
			case 6:	//Combobox
					return create_field_select($field);
			
			
			case 3:	//Radiobutton
			case 2: //Checkbox
					return create_field_checkbox($field);
			
			case 0: //None
        	case 1: //Pushbutton
					//return;
			
			case 4:	//Text
			case 7:	//Signature
			default: 
					return create_field_text($field);
           
        }			
}

function create_field_text($field){
	return	array (
		    'widget_type' => 'text',
		    'label' => strtolower($field['name']),
		    'weight' => (int)$field['weight'],
		    'rows' => '1',
		    'description' => '',
		    'default_value_widget' => 
		    array (
		      (string)$field['drupal_fieldname'] => 
		      array (
		        0 => 
		        array (
		          'value' => '',
		        ),
		      ),
		    ),
		    'default_value_php' => '',
		    'group' => false,
		    'required' => '0',
		    'multiple' => '0',
		    'text_processing' => '0',
		    'max_length' => '',
		    'allowed_values' => '',
		    'allowed_values_php' => '',
		    'field_name' => (string)$field['drupal_fieldname'],
		    'field_type' => 'text',
		    'module' => 'text',
		    'default_value' => 
		    array (
		      0 => 
		      array (
		        'value' => '',
		      ),
		    ),
		  );	
}



function create_field_select($field){	
return array (
    'widget_type' => 'options_select',
    'label' => strtolower($field['name']),
    'weight' => (int)$field['weight'],
    'description' => '',
    'default_value_widget' => 
    array (
      (string)$field['drupal_fieldname'] => 
      array (
        'key' => '',
      ),
    ),
    'default_value_php' => '',
    'group' => false,
    'required' => '0',
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => ''/*'val1
val2
val3'*/,
    'allowed_values_php' => '',
    'field_name' => (string)$field['drupal_fieldname'],
    'field_type' => 'text',
    'module' => 'text, optionwidgets',
  );
}

function create_field_checkbox($field){	
return  array (
    'widget_type' => 'options_onoff',
     'label' => strtolower($field['name']),
    'weight' => (int)$field['weight'],
    'description' => strtolower($field['name']),
    'default_value_widget' => 
    array (
      (string)$field['drupal_fieldname'] => 
      array (
        'keys' => 1,
      ),
    ),
    'default_value_php' => '',
    'group' => false,
    'required' => '0',
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'field_name' => (string)$field['drupal_fieldname'],
    'field_type' => 'text',
    'module' => 'text, optionwidgets',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => NULL,
      ),
    ),
  );
}

function create_field_button($field){	
	//probly don't want buttons, so no implementation
}

?>