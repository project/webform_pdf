-- SUMMARY --
This module allows users to upload PDFs, whose form-fields will be used to automatically generate a Webform.  Later, users can download the populated PDFs from Webform submissions.
This module is useful if you need to be able to dowload your Webform documents in PDF format, especially if you have to adhere to a strict document structure.

-- Dependencies --
Webform.module


-- INSTALLATION --
Install via sites/all/modules > Other > Webform PDF

-- USAGE --
* Go to /node/add/webform
* Click the "Generate From PDF" button at the top
* Select a PDF upload.  Make sure the PDF document isn't encrypted.  If it is encrypted and non copy-righted (typical of government PDFs), then try a decrypting tool like "Advanced PDF Password Recovery."  If you upload an encrypted PDF, you will have empty PDFs when you attempt to download your submissions. 
* When you have submissions that you'd like to download in PDF format, go to /webform_pdf/print_pdfs.  Currently this page is based on submissions per-user, I'll eventually work with this to make it more usable.

-- HELP DEVELOP!! --
This module is very much in development.  Most of the functionality is on MY home server in some Java iText classes and command-line PDFTK calls.  This is accessed using CGI calls from within this module, and this needs to change.  The only problem is that I haven't found any viable PHP solutions for parsing PDF form-fields, merging XFDF documents with PDF documents & flattening the results, etc.  It makes more sense to depend on service calls to my server than to expect users to have Java (iText) or PDFTK available on their hosting, so that's the bets interim... however, if any developers out there know of any PHP solutions to these issues, please contact me! 

-- CONTACT --
tylerrenelle@gmail.com